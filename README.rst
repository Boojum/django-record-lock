===========
Record Lock
===========

``record_lock`` is a Django app providing advisory per-record locks. For example,
you may want to mark an editable page as being "in use" so that authors can't
overwrite one-another. In this case, the second author would see a message
advising that the page was being edited already. "Advisory" means that the locks
can be overridden if you have lower-level access to the database.

This version has been written to be compatible with a prior PHP implementation
by Nathan Fraser, and may not yet take full advantage of Django features.


Testing
-------

To test, first install the package into your virtualenv, ensuring that Django is
also installed. Then run:

.. code-block:: bash

    $ python manage.py test record_lock
