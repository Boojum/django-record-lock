# -*- coding: utf-8 -*-
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Rowlock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tname', models.CharField(max_length=128)),
                ('rid', models.IntegerField()),
                ('sid', models.CharField(max_length=128)),
                ('ts', models.DateTimeField(auto_now_add=True)),
                ('oid', models.CharField(max_length=256, blank=True)),
            ],
            options={
                'db_table': 'rowlock',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='rowlock',
            unique_together=set([('tname', 'rid')]),
        ),
    ]
