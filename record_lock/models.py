import datetime

from django.db import models, IntegrityError

LOCK_EXPIRY = 900

class Rowlock(models.Model):
    """Advisory record locking.

     - tname : name of the resource/table to be accessed
     - rid : row identifier, must be type bigint (use 0 for non-tables)
     - sid : http session identifier string
     - ts : last access timestamp
     - oid : owner id string

    TODO: Perhaps instead use django-locking
    (http://stdbrouw.github.com/django-locking/).

    """
    tname = models.CharField(max_length=128)
    rid = models.IntegerField()
    sid = models.CharField(max_length=128)
    ts = models.DateTimeField(auto_now_add=True)
    oid = models.CharField(max_length=256, blank=True)

    class Meta:
        db_table = 'rowlock'
        unique_together = ('tname', 'rid')

    def __str__(self):
        return "%s (%s) locked %s #%d (%s sec ago)" % (self.oid, self.sid, self.tname, self.rid, self.age())

    def age(self):
        td = datetime.datetime.now() - self.ts
        #return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6
        return td.total_seconds()


class ReadOnly(Exception):
    pass


class LockingManager(models.Manager):
    """Manager to add advisory locking to a model."""

    def get_locked(self, owner_desc, owner_sid, *args, **kwargs):
        """Generic get row locked.

         - Attempt to obtain a row lock on the chosen row, then
           fetch the vo
         - If lock succeeds, caller's session will have a limited
           time, stateless lock on the named record until an unlock
           call is made or the lock times out.
         - If lock fails, row will still be returned but readonly
           will be set, and vo lock flags will indicate other owner.

        """
        obj = self.get(*args, **kwargs)
        if not obj.claim_lock(owner_desc, owner_sid):
            obj._readonly = True
        return obj


class LockingModel(models.Model):
    """Model with advisory locking features.

    This model is intended to replicate the DAO locking features from our
    original PHP code. It has been modified to be used in an object-oriented
    style, but essentially remains the same.

    Why does it remain the same, you ask? Surely it should be refactored since
    this is Python, not PHP? The answer is simply that coverting complex
    components such as Merge Manager is hard enough on its own, without
    refactoring.

    Models inheriting from this class have the following addition fields. These
    fields work similarly to the original PHP code implementing record locking.

     _readonly    true if record should not be written back to db
     _lock_held   True if calling process holds write lock
     _lock_tname  Rowlock.tname of lock
     _lock_rid    Rowlock.rid of lock or None
     _lock_sid    Rowlock.sid or None
     _lock_oid    Rowlock.oid or None

    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._readonly = False
        self._lock_held = False
        self._lock_tname = None
        self._lock_rid = None
        self._lock_sid = None
        self._lock_oid = None

    class Meta:
        abstract = True

    def is_lock_table(self):
        """Lookup a value in the lock tables map.

        The PHP code had an unused feature where any tables in a specified list
        are read-only if lock not explicitly requested (see
        dao_is_lock_table). This has not been implemented.

        """
        raise NotImplementedError()

    def claim_lock(self, owner_desc, owner_sid):
        """Claim a write lock on a named resource for the current session.

        - if object being locked is not table/row use rid = 0
        - on success, lock flags are updated, return is True
        - on fail, lock flags updated, return is False

        WARNING: As implemented in PHP, claim_lock does not set self._readonly
        on locking fail. This means you must ALWAYS check and respect the
        return value from claim_lock (ie. self.save() will still save).

        TODO: Add "for update" on the initial Rowlock.objects.get via Django's
        ORM once we've upgraded to 1.4.

        """
        table = self._meta.db_table
        record_id = self.pk

        try:
            rowlock = Rowlock.objects.get(tname=table, rid=record_id)
        except Rowlock.DoesNotExist:
            rowlock = None

        if rowlock:
            self._lock_tname = table
            self._lock_rid = record_id
            self._lock_held = (owner_sid == rowlock.sid
                               or rowlock.age() >= LOCK_EXPIRY + 60)
            if self._lock_held:
                if owner_sid != rowlock.sid:
                    # Got lock by expiry
                    self._lock_sid = owner_sid
                else:
                    # Already owned lock
                    self._lock_sid = rowlock.sid
                # Always write over a claimed lock's owner-id to reflect last
                # access and script.
                self._lock_oid = owner_desc
                self.save_lock()
            else:
                self._lock_sid = rowlock.sid
                self._lock_oid = rowlock.oid
        else:
            # Attempt to make a new lock record
            try:
                Rowlock.objects.create(tname=table, rid=record_id,
                                       sid=owner_sid, oid=owner_desc)
            except IntegrityError:
                # Assume fail was due to write conflict
                self._lock_held = False
            else:
                self._lock_held = True
                self._lock_tname = table
                self._lock_rid = record_id
                self._lock_sid = owner_sid
                self._lock_oid = owner_desc

        return self._lock_held

    def is_locked(self):
        """Is the vo locked, and is the lock held by the caller?"""
        return self._lock_held

    def has_lock(self):
        """Does the vo have a lock associated with it?"""
        return bool(self._lock_tname)

    def release_lock(self):
        """Release a lock on a named resource.

         - silently ignores vo if lock not held
         - on failure exception will be thrown

        """
        if self.is_locked():
            Rowlock.objects.filter(tname=self._lock_tname, rid=self._lock_rid).delete()
            self._lock_held = False
            self._lock_tname = None
            self._lock_rid = None
            self._lock_sid = None
            self._lock_oid = None
        return True

    def save_lock(self):
        """Write back a claimed lock.

        Called automatically by dao_save if required, but may be called
        manually if required on non-db resource

        """
        if self.is_locked():
            qs = Rowlock.objects.filter(tname=self._lock_tname, rid=self._lock_rid)
            qs.update(sid=self._lock_sid, oid=self._lock_oid, ts=datetime.datetime.now())
        return True

    def query_lock(self, owner_sid=None):
        """Generic lock query.

        Fills vo with lock fields if present in rowlock table, but no new lock
        is granted. Rows are selected for shared read, so this is purely for
        informational purposes and should _never_ be used to determine if a
        lock will be granted to this script invocation.

        Instead use dao_claim_lock, and then release if not needed.

        """
        table = self._meta.db_table
        record_id = self.pk

        try:
            rowlock = Rowlock.objects.get(tname=table, rid=record_id)
        except Rowlock.DoesNotExist:
            rowlock = None

        if rowlock and rowlock.age() < LOCK_EXPIRY + 60:
            self._lock_tname = table
            self._lock_rid = record_id
            self._lock_sid = rowlock.sid
            self._lock_oid = rowlock.oid
            self._lock_held = owner_sid == rowlock.sid

        return bool(self._lock_tname)

    def get_locked(self):
        raise NotImplementedError("Use get_locked() on the Manager instead.")

    def get_list_locks(self):
        # TODO: Not required by Merge Manager, so has not been implemented. May
        # be needed by Messages, but should perhaps be on the Manager class
        # anyway.
        raise NotImplementedError()

    def save(self, *args, **kwargs):
        if self._readonly:
            raise self.ReadOnly('Attempt to save read-only model instance.')
        super().save(*args, **kwargs)

    def lock_owner(self):
        return self._lock_oid

    ReadOnly = ReadOnly
