import datetime

from django.utils import unittest

from .models import Rowlock
from cam.models import Communication, Context

class LockingModelTestCase(unittest.TestCase):

    def setUp(self):
        self.c = Communication.objects.create()

    def test_claim_and_release(self):
        """Check status, before locking, after locking and after releasing."""

        # Check defaults
        self.assertEqual(self.c._readonly, False)
        self.assertEqual(self.c._lock_held, False)
        self.assertEqual(self.c._lock_tname, None)
        self.assertEqual(self.c._lock_rid, None)
        self.assertEqual(self.c._lock_sid, None)
        self.assertEqual(self.c._lock_oid, None)

        # Get a lock
        claimed = self.c.claim_lock('Lock Owner 1', 'aaa')
        self.assertTrue(claimed)

        # Check our lock state looks ok
        Rowlock.objects.get(tname=self.c._meta.db_table,
                            rid=self.c.communication_id,
                            sid='aaa',
                            oid='Lock Owner 1',
                            ts__gt=datetime.datetime.now() - datetime.timedelta(seconds=10),
                            ts__lt=datetime.datetime.now() + datetime.timedelta(seconds=10),)
        self.assertEqual(self.c._readonly, False)
        self.assertEqual(self.c._lock_held, True)
        self.assertEqual(self.c._lock_tname, self.c._meta.db_table)
        self.assertEqual(self.c._lock_rid, self.c.communication_id)
        self.assertEqual(self.c._lock_sid, 'aaa')
        self.assertEqual(self.c._lock_oid, 'Lock Owner 1')
        self.assertEqual(self.c.is_locked(), True)
        self.assertEqual(self.c.has_lock(), True)

        # Release the lock
        self.c.release_lock()

        # Check lock state looks ok
        with self.assertRaises(Rowlock.DoesNotExist):
            Rowlock.objects.get(tname=self.c._meta.db_table,
                                rid=self.c.communication_id)
        self.assertEqual(self.c._readonly, False)
        self.assertEqual(self.c._lock_held, False)
        self.assertEqual(self.c._lock_tname, None)
        self.assertEqual(self.c._lock_rid, None)
        self.assertEqual(self.c._lock_sid, None)
        self.assertEqual(self.c._lock_oid, None)
        self.assertEqual(self.c.is_locked(), False)
        self.assertEqual(self.c.has_lock(), False)

    def test_claim_expired(self):
        """Check you can claim over an expired lock."""

        self.c.claim_lock('Lock Owner 1', 'aaa')
        last_week = datetime.datetime.now() - datetime.timedelta(days=7)
        Rowlock.objects.filter(tname=self.c._meta.db_table,
                               rid=self.c.communication_id).update(ts=last_week)
        self.assertTrue(self.c.claim_lock('Lock Owner 2', 'bbb'))

    def test_reclaim_own_lock(self):
        """Check you can reclaim a lock you already hold."""

        self.c.claim_lock('Lock Owner 1', 'aaa')
        self.assertTrue(self.c.claim_lock('Lock Owner 1', 'aaa'))

    def test_claim_locked_record(self):
        """Check that attempting to lock another's locked record fails."""

        self.c.claim_lock('Lock Owner 1', 'aaa')

        c2 = Communication.objects.get(pk=self.c.communication_id)
        self.assertFalse(c2.claim_lock('Lock Owner 2', 'bbb'))
        Rowlock.objects.get(tname=self.c._meta.db_table,
                            rid=self.c.communication_id,
                            sid='aaa')


class LockingManagerTestCase(unittest.TestCase):
    def setUp(self):
        # TODO: Remove zero context when we've fixed Communication.context to
        # default to None.
        Context.objects.create(context_id=0, name='Default')
        self.c = Communication.objects.create()

    def test_get_locked(self):
        c1 = Communication.objects.get_locked('Lock Owner 1', 'aaa', pk=self.c.communication_id)
        self.assertTrue(c1.is_locked())

        c2 = Communication.objects.get_locked('Lock Owner 2', 'bbb', pk=self.c.communication_id)
        self.assertFalse(c2.is_locked())
        self.assertTrue(c2._readonly)

        with self.assertRaises(Communication.ReadOnly):
            c2.save()
